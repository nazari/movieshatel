package com.shatel.namavatest.di

import com.shatel.namavatest.di.clients.*
import com.shatel.namavatest.network.ApiRepository
import com.shatel.namavatest.network.errorhandling.ResponseHandler
import com.shatel.namavatest.ui.detail.DetailViewModel
import com.shatel.namavatest.ui.home.HomeViewModel
import com.shatel.namavatest.ui.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val networkModule = module {

    single { provideLoggingInterceptor() }
    single { ResponseHandler(get()) }
    single { provideOkHttpClient(get()) }
    single { provideApiService(get()) }
    single { provideRetrofit(get(), get()) }
    single { ApiRepository(get(), get()) }
    single { provideMoshi() }
}

val applicationModules = module {
    single { provideExoPlayer(get()) }
    single { provideMediaSourceFactory(get()) }
}

val viewModelModules = module {
    viewModel { MainViewModel(get()) }
    viewModel { DetailViewModel() }
    viewModel { HomeViewModel(get()) }
}