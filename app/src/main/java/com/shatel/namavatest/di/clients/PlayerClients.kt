package com.shatel.namavatest.di.clients

import android.content.Context
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.shatel.namavatest.R


fun provideExoPlayer(context: Context): ExoPlayer {
    return ExoPlayerFactory.newSimpleInstance(context)
}

fun provideMediaSourceFactory(context: Context): DataSource.Factory {
    return DefaultDataSourceFactory(context, Util.getUserAgent(context, context.getString(R.string.app_name)))
}