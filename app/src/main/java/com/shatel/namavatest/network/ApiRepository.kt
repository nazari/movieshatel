package com.shatel.namavatest.network

import com.shatel.namavatest.model.search.ResponseSearch
import com.shatel.namavatest.network.errorhandling.Resource
import com.shatel.namavatest.network.errorhandling.ResponseHandler
import java.lang.Exception


class ApiRepository(private val apiService: ApiService, private val responseHandler: ResponseHandler) {


    suspend fun getSearchData(searchQuery: String): Resource<ResponseSearch> {
        return try {
           val response = apiService.getSearchData(searchQuery)
            responseHandler.handleSuccess(response)
        } catch (e: Exception) {
            e.printStackTrace()
            responseHandler.handleException(e)
        }
    }
}