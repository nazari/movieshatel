package com.shatel.namavatest.network.errorhandling

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
