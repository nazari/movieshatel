package com.shatel.namavatest.network

import com.shatel.namavatest.model.search.ResponseSearch
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/videos")
    suspend fun getSearchData(@Query("query") searchQuery: String): ResponseSearch

}