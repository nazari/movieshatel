package com.shatel.namavatest.network.errorhandling



import android.content.Context
import com.shatel.namavatest.R
import retrofit2.HttpException
import timber.log.Timber
import java.net.SocketTimeoutException
import java.util.concurrent.CancellationException
const val CANCEL = "cancel"

open class ResponseHandler(private val context: Context) {
    fun <T : Any> handleSuccess(data: T): Resource<T> {
        return Resource.success("success",data)
    }

    fun <T : Any> handleException(e: Exception): Resource<T> {
        Timber.e(e)
        return when (e) {
            is HttpException -> Resource.error(getErrorMessage(e.code()), null)
            is SocketTimeoutException -> Resource.error(context.getString(R.string.timeOut_error), null)
            is CancellationException -> Resource.error(CANCEL ,null)
            else -> Resource.error(getErrorMessage(Int.MAX_VALUE), null)
        }
    }

    private fun getErrorMessage(code: Int): String {
        return when (code) {
            401 -> "Unauthorised"
            404 -> "Not found"
            else -> "Something went wrong"
        }
    }
}