package com.shatel.namavatest.utils.baseclasses

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shatel.namavatest.utils.liveDataHandler.Event
import org.koin.android.ext.android.inject


open class BaseViewModel : ViewModel() {

    val isShowLoading = MutableLiveData<Boolean>(false)
    val isShowRetry = MutableLiveData<Boolean>(false)
    val errorMessage = MutableLiveData(Event(""))
}
