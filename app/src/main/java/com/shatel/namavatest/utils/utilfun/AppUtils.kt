package com.shatel.namavatest.utils.utilfun

import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.shatel.namavatest.R



// get Color Resource with string
fun getColorByString(context: Context, colorName: String): Int {
    return try {
        context.resources.getIdentifier(
            colorName,
            "color",
            context.packageName
        )
    }catch (e:Exception){
        e.printStackTrace()
        R.color.colorAccent
    }

}

fun hideKeyboard(activity: Activity) {
    val inputManager: InputMethodManager = activity
        .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

    // check if no view has focus:
    val currentFocusedView = activity.currentFocus
    if (currentFocusedView != null) {
        inputManager.hideSoftInputFromWindow(
            currentFocusedView.windowToken,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }
}


@TargetApi(Build.VERSION_CODES.M)
fun requestPermission(
    activity: Activity,
    permissions: Array<String?>?,
    requestCode: Int

) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (permissions != null) {
            activity.requestPermissions(permissions, requestCode);
        }
    }
}

@TargetApi(Build.VERSION_CODES.M)
fun hasPermission(activity: Activity, permission: String?): Boolean {
    return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
            permission?.let { activity.checkSelfPermission(it) } == PackageManager.PERMISSION_GRANTED
}

fun makeFullScreen(activity:Activity) {
    val decorView: View = activity.window.decorView
    decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            or View.SYSTEM_UI_FLAG_FULLSCREEN
            or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
}