package com.shatel.namavatest.utils.utilfun

import android.content.res.Resources
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

// convert dp to px and px to dp
val Int.dp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()
val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()


fun View.setPaddingInDp(left: Int, top: Int, right: Int, bottom: Int) {
    this.setPadding(left.px, top.px, right.px, bottom.px)
}

fun String?.toFaNum(): String {
    return if (this != null) {
        this.replace("0", "۰")
            .replace("1", "۱")
            .replace("2", "۲")
            .replace("3", "۳")
            .replace("4", "۴")
            .replace("5", "۵")
            .replace("6", "۶")
            .replace("7", "۷")
            .replace("8", "۸")
            .replace("9", "۹")
    } else "-"
}

//add debounce for live search
fun <T> LiveData<T>.debounce(duration: Long = 500L) = MediatorLiveData<T>().also { mld ->
    val source = this
    val handler = Handler(Looper.getMainLooper())

    val runnable = Runnable {
        mld.value = source.value
    }

    mld.addSource(source) {
        handler.removeCallbacks(runnable)
        handler.postDelayed(runnable, duration)
    }
}
