package com.shatel.namavatest.utils.baseclasses

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.shatel.namavatest.BR
import com.shatel.namavatest.R
import com.shatel.namavatest.utils.liveDataHandler.EventObserver


abstract class BaseFragment<B : ViewDataBinding, VM : BaseViewModel>(@LayoutRes private val layoutResourceId: Int) :
    Fragment() {

    protected abstract val viewModel: VM
    lateinit var binding: B


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, layoutResourceId, container, false)
        return binding.root
    }

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.lifecycleOwner = viewLifecycleOwner
        binding.setVariable(BR.viewModel, viewModel)

        viewModel.errorMessage.observe(viewLifecycleOwner, EventObserver {
           if (it.isNotEmpty()){
               showError(it)
           }
        })
        init()
    }

    abstract fun init()


    open fun showMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    open fun showError(message: String?) {
        showMessage(message ?: requireContext().getString(R.string.general_error))
    }

    open fun showLoading() {
        viewModel.isShowLoading.value = true
    }

    open fun hideLoading() {
        viewModel.isShowLoading.value = false
    }
}

