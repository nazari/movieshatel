package com.shatel.namavatest.utils.utilfun

import android.graphics.drawable.Drawable
import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.shatel.namavatest.R

@BindingAdapter("isVisible")
fun View.isVisible(isVisible: Boolean) {
    if (isVisible) this.visibility = View.VISIBLE else this.visibility = View.INVISIBLE
}

@BindingAdapter("isGone")
fun View.isGone(isGone: Boolean) {
    if (isGone) this.visibility = View.GONE else this.visibility = View.VISIBLE
}

//load image url directly with glide
@BindingAdapter("imageUrl")
fun ImageView.setImageUrl(url: String?) {
    Glide.with(context).load(url).placeholder(R.drawable.video_placeholder)
        .into(this)

}

