package com.shatel.namavatest.ui.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shatel.namavatest.model.search.Data
import com.shatel.namavatest.utils.baseclasses.BaseViewModel
import com.shatel.namavatest.utils.liveDataHandler.Event

class DetailViewModel : BaseViewModel() {
    val videoData = MutableLiveData<Data>()
    val isPlayingVideo = MutableLiveData(false)
}