package com.shatel.namavatest.ui.home

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import com.shatel.namavatest.model.search.ResponseSearch
import com.shatel.namavatest.network.ApiRepository
import com.shatel.namavatest.network.errorhandling.CANCEL
import com.shatel.namavatest.network.errorhandling.Resource
import com.shatel.namavatest.network.errorhandling.Status
import com.shatel.namavatest.utils.baseclasses.BaseViewModel
import com.shatel.namavatest.utils.liveDataHandler.Event
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import timber.log.Timber

class HomeViewModel(val apiRepository: ApiRepository) : BaseViewModel() {

    val searchWord = MutableLiveData("")
    val isShowRecyclerPlaceHolder = MutableLiveData(true)
    val eventSearchWord = Transformations.map(searchWord) {
        return@map Event(it)
    }

    private val searchResponseData = MutableLiveData<Resource<ResponseSearch>>()
    val searchResponse = MediatorLiveData<ResponseSearch>()
    private var job: Job? = null

    init {

        searchResponse.addSource(searchResponseData) {
            when (it.status) {
                Status.LOADING -> isShowLoading.value = true

                Status.SUCCESS -> {
                    searchResponse.value = it.data
                    isShowRecyclerPlaceHolder.value = it.data?.data?.isNotEmpty() != true
                    isShowLoading.value = false
                }
                Status.ERROR -> {
                    if (it.message != CANCEL){
                        isShowRetry.value = true
                        isShowLoading.value = false
                        errorMessage.value = Event(it.message ?: "")
                    }
                }
            }
        }

    }

    fun callSearchApiRequest() {
        isShowRetry.value = false
        searchWord.value?.let { word ->
            if (word.isNotEmpty()) {
                searchResponseData.value = Resource.loading(null)
                job?.cancel()
                job = viewModelScope.launch(Dispatchers.IO) {
                    Timber.i("searchWord $word")
                    searchResponseData.postValue(apiRepository.getSearchData(word))
                }
            }
        }


    }

}