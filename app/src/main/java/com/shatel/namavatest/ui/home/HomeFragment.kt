package com.shatel.namavatest.ui.home

import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.shatel.namavatest.R
import com.shatel.namavatest.databinding.HomeFragmentBinding
import com.shatel.namavatest.model.search.Data
import com.shatel.namavatest.ui.home.adapter.SearchItemListener
import com.shatel.namavatest.ui.home.adapter.SearchRecyclerAdapter
import com.shatel.namavatest.ui.home.adapter.SpacesItemDecoration
import com.shatel.namavatest.utils.baseclasses.BaseFragment
import com.shatel.namavatest.utils.liveDataHandler.EventObserver
import com.shatel.namavatest.utils.utilfun.debounce
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : BaseFragment<HomeFragmentBinding, HomeViewModel>(R.layout.home_fragment),SearchItemListener {

    override val viewModel: HomeViewModel by viewModel()
    lateinit var adapter: SearchRecyclerAdapter
    override fun init() {

        viewModel.eventSearchWord.debounce(1000).observe(viewLifecycleOwner, EventObserver {
            if (it.isNotEmpty()) viewModel.callSearchApiRequest()
        })

        initRecycler()


        viewModel.searchResponse.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it.data)
            adapter.notifyDataSetChanged()
        })

    }

    private fun initRecycler() {
        adapter = SearchRecyclerAdapter(this)
        binding.rvSearch.adapter = adapter
        binding.rvSearch.addItemDecoration(SpacesItemDecoration(requireContext(), R.dimen.item_offset))
        binding.rvSearch.layoutManager = GridLayoutManager(requireContext(),2)
    }

    private fun openDetailFragment(item: Data) {
        findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToDetailFragment(item))
    }

    override fun onSearchItemClick(item: Data) {
        openDetailFragment(item)
    }


}