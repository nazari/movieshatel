package com.shatel.namavatest.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.shatel.namavatest.databinding.ItemRvSearchBinding
import com.shatel.namavatest.model.search.Data
import com.shatel.namavatest.utils.utilfun.setImageUrl

class SearchRecyclerAdapter(val searchItemListener: SearchItemListener) :
    ListAdapter<Data, SearchRecyclerAdapter.SearchViewHolder>(
        SearchDiffUtil()
    ) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemRvSearchBinding.inflate(inflater, parent, false)
        return SearchViewHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        val item = getItem(position)
        item.pictures?.sizes?.get(0)?.link.let {
            holder.bindig.image.setImageUrl(it)
        }
        holder.bindig.root.setOnClickListener {
            searchItemListener.onSearchItemClick(item)
        }
    }

    class SearchViewHolder(val bindig: ItemRvSearchBinding) : RecyclerView.ViewHolder(bindig.root)

}


class SearchDiffUtil : DiffUtil.ItemCallback<Data>() {
    override fun areItemsTheSame(oldItem: Data, newItem: Data): Boolean {
       return oldItem.uri == newItem.uri
    }

    override fun areContentsTheSame(oldItem: Data, newItem: Data): Boolean {
       return oldItem == newItem
    }

}

interface SearchItemListener{
    fun onSearchItemClick(item:Data)
}