package com.shatel.namavatest.ui.detail

import android.app.Activity
import android.graphics.Color
import android.net.Uri
import android.view.View
import androidx.lifecycle.Observer
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.upstream.DataSource
import com.shatel.namavatest.R
import com.shatel.namavatest.databinding.DetailFragmentBinding
import com.shatel.namavatest.utils.baseclasses.BaseFragment
import com.shatel.namavatest.utils.utilfun.makeFullScreen
import kotlinx.android.synthetic.main.detail_fragment.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel


class DetailFragment :
    BaseFragment<DetailFragmentBinding, DetailViewModel>(R.layout.detail_fragment) {


    private val exoPlayer: ExoPlayer by inject()
    private val mediaDataSourceFactory: DataSource.Factory by inject()
    override val viewModel: DetailViewModel by viewModel()
    override fun init() {

        viewModel.videoData.value = (DetailFragmentArgs.fromBundle(requireArguments()).data)

        binding.imgPlay.setOnClickListener{
            viewModel.isPlayingVideo.value = true
        }

        viewModel.isPlayingVideo.observe(viewLifecycleOwner, Observer {
            if (it){
                initialPlayer()
            }
        })
    }


    private fun initialPlayer() {
        val mediaSource = ProgressiveMediaSource.Factory(mediaDataSourceFactory).createMediaSource(
            Uri.parse("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
        )

        exoPlayer.prepare(
            mediaSource, false, false
        )

        exoPlayer.playWhenReady = true

        if (requireContext().resources?.getBoolean(R.bool.full_screen_mode) == true) {
            makeFullScreen(requireActivity())
        }
        playerView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FILL
        binding.playerView.setShutterBackgroundColor(Color.TRANSPARENT)
        binding.playerView.player = exoPlayer
        binding.playerView.requestFocus()

    }



    override fun onPause() {
        super.onPause()
        exoPlayer.playWhenReady = false;

    }


}