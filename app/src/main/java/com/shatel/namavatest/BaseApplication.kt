package com.shatel.namavatest

import android.app.Application
import com.shatel.namavatest.BuildConfig
import com.shatel.namavatest.di.applicationModules
import com.shatel.namavatest.di.networkModule
import com.shatel.namavatest.di.viewModelModules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber


class BaseApplication: Application(){

    override fun onCreate() {
        super.onCreate()


        //start Timber
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        // start Koin!
        startKoin {
            androidLogger()
            androidContext(this@BaseApplication)
            modules(networkModule, viewModelModules, applicationModules)
        }

    }


}