package com.shatel.namavatest.model.search


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
import com.shatel.namavatest.model.search.Badges

@Parcelize
data class Embed(
    @Json(name = "badges")
    val badges: Badges?,
    @Json(name = "html")
    val html: String?
) : Parcelable