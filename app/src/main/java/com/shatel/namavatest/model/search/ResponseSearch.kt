package com.shatel.namavatest.model.search


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class ResponseSearch(
    @Json(name = "data")
    val data: List<Data>?,
    @Json(name = "page")
    val page: Int?,
    @Json(name = "paging")
    val paging: Paging?,
    @Json(name = "per_page")
    val perPage: Int?,
    @Json(name = "total")
    val total: Int?
) : Parcelable