package com.shatel.namavatest.model.search


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class LocationDetails(
    @Json(name = "city")
    val city: String?,
    @Json(name = "country")
    val country: String?,
    @Json(name = "country_iso_code")
    val countryIsoCode: String?,
    @Json(name = "formatted_address")
    val formattedAddress: String?,
    @Json(name = "latitude")
    val latitude: String?,
    @Json(name = "longitude")
    val longitude: String?,
    @Json(name = "neighborhood")
    val neighborhood: String?,
    @Json(name = "state")
    val state: String?,
    @Json(name = "state_iso_code")
    val stateIsoCode: String?,
    @Json(name = "sub_locality")
    val subLocality: String?
) : Parcelable