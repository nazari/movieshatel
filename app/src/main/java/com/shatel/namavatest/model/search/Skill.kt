package com.shatel.namavatest.model.search


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class Skill(
    @Json(name = "name")
    val name: String?,
    @Json(name = "uri")
    val uri: String?
) : Parcelable