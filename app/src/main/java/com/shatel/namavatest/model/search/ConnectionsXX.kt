package com.shatel.namavatest.model.search


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class ConnectionsXX(
    @Json(name = "albums")
    val albums: Albums?,
    @Json(name = "appearances")
    val appearances: Appearances?,
    @Json(name = "channels")
    val channels: Channels?,
    @Json(name = "feed")
    val feed: Feed?,
    @Json(name = "folders_root")
    val foldersRoot: FoldersRoot?,
    @Json(name = "followers")
    val followers: Followers?,
    @Json(name = "following")
    val following: Following?,
    @Json(name = "groups")
    val groups: Groups?,
    @Json(name = "likes")
    val likes: Likes?,
    @Json(name = "membership")
    val membership: Membership?,
    @Json(name = "moderated_channels")
    val moderatedChannels: ModeratedChannels?,
    @Json(name = "pictures")
    val pictures: Pictures?,
    @Json(name = "portfolios")
    val portfolios: Portfolios?,
    @Json(name = "shared")
    val shared: Shared?,
    @Json(name = "videos")
    val videos: Videos?
) : Parcelable