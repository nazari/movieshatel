package com.shatel.namavatest.model.search


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class ConnectionsX(
    @Json(name = "videos")
    val videos: Videos?
) : Parcelable