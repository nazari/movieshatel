package com.shatel.namavatest.model.search


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class Interactions(
    @Json(name = "report")
    val report: Report?
) : Parcelable