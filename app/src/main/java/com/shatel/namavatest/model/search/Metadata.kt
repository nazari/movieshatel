package com.shatel.namavatest.model.search


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
import com.shatel.namavatest.model.search.Connections

@Parcelize
data class Metadata(
    @Json(name = "connections")
    val connections: Connections?,
    @Json(name = "interactions")
    val interactions: Interactions?
) : Parcelable