package com.shatel.namavatest.model.search


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class Connections(
    @Json(name = "comments")
    val comments: Comments?,
    @Json(name = "credits")
    val credits: Credits?,
    @Json(name = "likes")
    val likes: Likes?,
    @Json(name = "pictures")
    val pictures: Pictures?,
    @Json(name = "recommendations")
    val recommendations: Recommendations?,
    @Json(name = "related")
    val related: Related?,
    @Json(name = "texttracks")
    val texttracks: Texttracks?
) : Parcelable