package com.shatel.namavatest.model.search


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class PicturesX(
    @Json(name = "active")
    val active: Boolean?,
    @Json(name = "resource_key")
    val resourceKey: String?,
    @Json(name = "sizes")
    val sizes: List<Size>?,
    @Json(name = "type")
    val type: String?,
    @Json(name = "uri")
    val uri: String?
) : Parcelable