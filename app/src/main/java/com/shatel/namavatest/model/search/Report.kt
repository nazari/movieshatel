package com.shatel.namavatest.model.search


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class Report(
    @Json(name = "options")
    val options: List<String>?,
    @Json(name = "reason")
    val reason: List<String>?,
    @Json(name = "uri")
    val uri: String?
) : Parcelable