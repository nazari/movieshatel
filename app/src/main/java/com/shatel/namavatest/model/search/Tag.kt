package com.shatel.namavatest.model.search


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class Tag(
    @Json(name = "canonical")
    val canonical: String?,
    @Json(name = "metadata")
    val metadata: Metadata?,
    @Json(name = "name")
    val name: String?,
    @Json(name = "resource_key")
    val resourceKey: String?,
    @Json(name = "tag")
    val tag: String?,
    @Json(name = "uri")
    val uri: String?
) : Parcelable