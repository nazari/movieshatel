package com.shatel.namavatest.model.search


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class Live(
    @Json(name = "archived")
    val archived: Boolean?,
    @Json(name = "streaming")
    val streaming: Boolean?
) : Parcelable