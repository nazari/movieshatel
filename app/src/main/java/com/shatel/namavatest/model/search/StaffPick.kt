package com.shatel.namavatest.model.search


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class StaffPick(
    @Json(name = "best_of_the_month")
    val bestOfTheMonth: Boolean?,
    @Json(name = "best_of_the_year")
    val bestOfTheYear: Boolean?,
    @Json(name = "normal")
    val normal: Boolean?,
    @Json(name = "premiere")
    val premiere: Boolean?
) : Parcelable