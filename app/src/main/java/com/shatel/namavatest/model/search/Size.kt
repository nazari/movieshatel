package com.shatel.namavatest.model.search


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class Size(
    @Json(name = "height")
    val height: Int?,
    @Json(name = "link")
    val link: String?,
    @Json(name = "link_with_play_button")
    val linkWithPlayButton: String?,
    @Json(name = "width")
    val width: Int?
) : Parcelable