package com.shatel.namavatest.model.search


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable


@Parcelize
data class Privacy(
    @Json(name = "add")
    val add: Boolean?,
    @Json(name = "comments")
    val comments: String?,
    @Json(name = "download")
    val download: Boolean?,
    @Json(name = "embed")
    val embed: String?,
    @Json(name = "view")
    val view: String?
) : Parcelable