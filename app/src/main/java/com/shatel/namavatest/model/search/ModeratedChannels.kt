package com.shatel.namavatest.model.search


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class ModeratedChannels(
    @Json(name = "options")
    val options: List<String>?,
    @Json(name = "total")
    val total: Int?,
    @Json(name = "uri")
    val uri: String?
) : Parcelable