package com.shatel.namavatest.model.search


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class Badges(
    @Json(name = "hdr")
    val hdr: Boolean?,
    @Json(name = "live")
    val live: Live?,
    @Json(name = "staff_pick")
    val staffPick: StaffPick?,
    @Json(name = "vod")
    val vod: Boolean?,
    @Json(name = "weekend_challenge")
    val weekendChallenge: Boolean?
) : Parcelable