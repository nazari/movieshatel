package com.shatel.namavatest.model.search


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class Paging(
    @Json(name = "first")
    val first: String?,
    @Json(name = "last")
    val last: String?,
    @Json(name = "next")
    val next: String?
) : Parcelable