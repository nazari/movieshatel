package com.shatel.namavatest.model.search


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class User(
    @Json(name = "account")
    val account: String?,
    @Json(name = "available_for_hire")
    val availableForHire: Boolean?,
    @Json(name = "bio")
    val bio: String?,
    @Json(name = "can_work_remotely")
    val canWorkRemotely: Boolean?,
    @Json(name = "created_time")
    val createdTime: String?,
    @Json(name = "gender")
    val gender: String?,
    @Json(name = "link")
    val link: String?,
    @Json(name = "location")
    val location: String?,
    @Json(name = "location_details")
    val locationDetails: LocationDetails?,
    @Json(name = "metadata")
    val metadata: Metadata?,
    @Json(name = "name")
    val name: String?,
    @Json(name = "pictures")
    val pictures: PicturesX?,
    @Json(name = "resource_key")
    val resourceKey: String?,
    @Json(name = "short_bio")
    val shortBio: String?,
//    @Json(name = "skills")
//    val skills: List<Skill>?,
    @Json(name = "uri")
    val uri: String?,
    @Json(name = "websites")
    val websites: List<Website>?
) : Parcelable