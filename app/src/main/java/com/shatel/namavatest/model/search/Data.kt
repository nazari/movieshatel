package com.shatel.namavatest.model.search


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class Data(
    @Json(name = "content_rating")
    val contentRating: List<String>?,
    @Json(name = "created_time")
    val createdTime: String?,
    @Json(name = "description")
    val description: String?,
    @Json(name = "duration")
    val duration: Int?,
    @Json(name = "embed")
    val embed: Embed?,
    @Json(name = "height")
    val height: Int?,
    @Json(name = "language")
    val language: String?,
    @Json(name = "license")
    val license: String?,
    @Json(name = "link")
    val link: String?,
    @Json(name = "metadata")
    val metadata: Metadata?,
    @Json(name = "modified_time")
    val modifiedTime: String?,
    @Json(name = "name")
    val name: String?,
    @Json(name = "pictures")
    val pictures: PicturesX?,
    @Json(name = "privacy")
    val privacy: Privacy?,
    @Json(name = "release_time")
    val releaseTime: String?,
    @Json(name = "resource_key")
    val resourceKey: String?,
    @Json(name = "stats")
    val stats: Stats?,
    @Json(name = "status")
    val status: String?,
    @Json(name = "transcode")
    val transcode: String?,
    @Json(name = "type")
    val type: String?,
    @Json(name = "upload")
    val upload: String?,
    @Json(name = "uri")
    val uri: String?,
    @Json(name = "user")
    val user: User?,
    @Json(name = "width")
    val width: Int?
) : Parcelable